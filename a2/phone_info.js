function fetchBrands(){
    console.log("fetching brands...");
    const endpoint = "https://api-mobilespecs.azharimm.site/v2/brands";

    fetch(endpoint)
    .then(response =>{
        return response.json();
    })
    .then(info => {
        console.dir(info);
        info.data.forEach(element => {
            createDropdownElement(element.brand_name, element.brand_slug);
        });
    })
    .catch(err => {
        console.log(err);
    })
}

function createDropdownElement(_dropdownItem, _brandSlug){
    console.log("creating dropdown element...");
    let dropdownItem = document.createElement("option");
    dropdownItem.innerHTML = _dropdownItem;
    dropdownItem.id = _brandSlug;
    document.getElementById("dropdown_phones").appendChild(dropdownItem)
}

function clickButton(){
    document.getElementById("input_search").addEventListener("click", () => {
        refreshMobilePhones();
        refreshMobilePhonesImage();
        fetchModels();
    })
}

function refreshMobilePhones(){
    document.getElementById("div_mobilePhones").innerHTML = "";
}

function refreshMobilePhonesImage(){
    document.getElementById("div_mobilePhonesImage").innerHTML = "";
}

function fetchModels(){
    console.log("fetching list information...");
    const endpoint = "https://api-mobilespecs.azharimm.site/v2/brands/" + getBrandSlug();

    fetch(endpoint)
    .then(response =>{
        return response.json();
    })
    .then(info => {
        console.dir(info);
        info.data.phones.forEach(element => {
            createListElement(element.phone_name, element.slug);
        });
    })
    .catch(err => {
        console.log(err);
    })
}

function getBrandSlug(){
    console.log("get brand slug...");
    let element =  document.getElementById("dropdown_phones");
    return element.options[element.selectedIndex].id;
}

function createListElement(_listItem, _phoneSlug){
    console.log("creating list element...");
    let dropdownItem = document.createElement("button");
    dropdownItem.innerHTML = _listItem;
    dropdownItem.classList = "list-group-item list-group-item-action";
    dropdownItem.id = _phoneSlug;
    document.getElementById("div_mobilePhones").appendChild(dropdownItem);
}

function clickListItem(){
    document.getElementById("div_mobilePhones").addEventListener("click", event => {
        let text = event.target.id;
        fetchPhoneInformation(text);
    });
}

function fetchPhoneInformation(_id){
    refreshMobilePhones();
    refreshMobilePhonesImage();
    console.log("fetching table information...");
    const endpoint = "https://api-mobilespecs.azharimm.site/v2/" + _id;
    fetch(endpoint)
    .then(response =>{
        return response.json();
    })
    .then(info => {
        createPhoneInformation(info.data.brand, "Marke");
        createPhoneInformation(info.data.phone_name, "Modell");
        createPhoneInformation(info.data.os, "Betriebssystem");
        createPhoneInformation(info.data.release_date, "Erscheinungsdatum");
        createPhoneImage(info.data.phone_images);
    })
    .catch(err => {
        console.log(err);
    })
}

function createPhoneInformation(_element, _name){
    console.log("creating list element...");
    let dropdownItem = document.createElement("p");
    dropdownItem.innerHTML = "<b>" + _name + "</b>: " + _element;
    document.getElementById("div_mobilePhones").appendChild(dropdownItem);
}

function createPhoneImage(_listItem, _phoneSlug){
    console.log("creating list element...");
    let dropdownItem = document.createElement("img");
    dropdownItem.src = _listItem[0];
    dropdownItem.style.maxHeight = "80%";
    dropdownItem.style.maxWidth = "80%";
    document.getElementById("div_mobilePhonesImage").appendChild(dropdownItem);
}

fetchBrands();
clickButton();
clickListItem();