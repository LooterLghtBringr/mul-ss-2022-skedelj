function clickButton(_info){
    document.getElementById("input_search").addEventListener("click", event => {
        fetchListInformation(getLocation());
    })
}

function getLocation(){
    console.log("get location");
    return document.getElementById("input_txt").value;
}

function fetchListInformation(_location){
    console.log("fetching list information...");
    const endpoint = "https://api.coinbase.com/v2/prices/"+_location+"/buy";
    //const endpoint = "http://api.deutschebahn.com/freeplan/v1/location/" + _location;

    fetch(endpoint)
    .then(response =>{
        return response.json();
    })
    .then(info => {
        console.dir(info);
        createListElement(info.data.base +" "+ info.data.amount);
    })
    .catch(err => {
        console.log(err);
    })
}

function createListElement(_listItem){
    console.log("creating list element...");
    let listItem = document.createElement("button");
    listItem.innerHTML = _listItem;
    listItem.classList = "list-group-item list-group-item-action";
    document.getElementById("div_trainStations").appendChild(listItem);
}

function clickListItem(){
    document.getElementById("div_trainStations").addEventListener("click", event => {
        let text = event.target.innerHTML.substring(0,3);
        console.log(text);
        fetchTableInformation(text);
    });
}

function fetchTableInformation(_id){
    console.log("fetching table information...");
    const endpoint = "https://api.coinbase.com/v2/exchange-rates?currency=" + _id;
    //const endpoint = "http://api.deutschebahn.com/freeplan/v1/journeyDetails/" + _id;
    fetch(endpoint)
    .then(response =>{
        return response.json();
    })
    .then(info => {
        console.dir(info);
        createTableElement(info);
    })
    .catch(err => {
        console.log(err);
    })
}

function createTableElement(_tableRow){
    console.log("creating table element");
    let row = document.getElementById("table_body").insertRow(0);
    let cellName = row.insertCell(0);
    let cellDate = row.insertCell(1);
    let cellDestination = row.insertCell(2);
    let cellTrainStation = row.insertCell(3);
    console.dir(_tableRow.data.rates.USD);
    cellName.innerHTML = _tableRow.data.rates.USD;
    cellDate.innerHTML = _tableRow.data.rates.EUR;
    cellDestination.innerHTML = _tableRow.data.rates.BTC;
    cellTrainStation.innerHTML = _tableRow.data.rates.ETH;
}

clickButton();
clickListItem();