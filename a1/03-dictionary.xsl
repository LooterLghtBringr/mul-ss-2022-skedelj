<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/dictionary">
        <html>
            <head>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"/>
                <script src="https://kit.fontawesome.com/d66eb23119.js" crossorigin="anonymous"></script>
                    <style>
                        header{
                            background-color: rgb(0,0,0);
                            height: 70rem;
                            text-align: center;

                            transition: background-color 2s;
                        }

                        header:hover{
                            background-color: rgb(35,35,35); 
                        }

                        h1{
                            color: white;
                            font-size: 120;
                            padding-top: 23rem;
                        }

                        h2{
                            color: white;
                            font-size: 120;
                            padding-top: 19rem;
                        }

                        p{
                            color: black;
                            font-size: 50;
                            padding-top: 1rem;
                        }

                        footer{
                            background-color: rgb(190,190,190);
                            height: 70rem;
                            text-align: center;

                            transition: background-color 2s;
                        }

                        footer:hover{
                            background-color: rgb(120,120,120);
                        }

                        i{
                            padding-right: 5px;
                        }

                        .table{
                            height: 60rem;
                        }

                        td, th{
                            font-size: 2rem;
                        }

                        tr{
                            transition: background-color 1s;
                        }

                        tr:hover{
                            background-color: lightblue;
                        }

                        h7{
                            position: relative;
                            top: 10rem;
                            bottom: 0rem;
                        }
                    </style>
            </head>

            <body>
            <!--HEADER-->
                <header>
                    <div class="container-fluid">
                        <h1>Wörterliste</h1>
                    </div>
                </header>

            <!--TABLE-->
                <div class="container-fluid">
                    <table class="table table-striped">
                        <thead class="table-light">
                            <th>English</th>
                            <th>Deutsch</th>
                            <th>Kategorie</th>
                        </thead>

                        <xsl:for-each select="word">
                            <xsl:sort select="@value" order="descending"></xsl:sort>
                            <tr>
                                <td> <xsl:value-of select="@value"/> </td>
                                <td> <xsl:value-of select="translation[@lang='DE']"/> </td>
                                <td>
                                    <xsl:choose>
                                        <xsl:when test="category = 'Animal'">
                                            <i class="fa-solid fa-dog"></i>
                                        </xsl:when>
                                        <xsl:when test="category = 'Culture'">
                                            <i class="fa-solid fa-palette"></i>
                                        </xsl:when>
                                        <xsl:when test="category = 'Geography'">
                                            <i class="fa-solid fa-book-atlas"></i>
                                        </xsl:when>
                                        <xsl:when test="category = 'Food'">
                                            <i class="fa-solid fa-burger"></i>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <i class="fa-solid fa-not-equal"></i>
                                        </xsl:otherwise>                    
                                    </xsl:choose>
                                
                                    <xsl:value-of select="category"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </table>
                </div>

            <!--FOOTER-->
            <footer>
                <div class="container-fluid">
                    <h2>Statistik</h2>
                    <p> Die Liste enthält insgesamt <xsl:value-of select="count(//word)"/> <strong> Vokabeln</strong></p>
                    <p> Deutsch <xsl:value-of select="count(/dictionary/word/translation[@lang='DE'])"/> </p>
                    <p> Französisch <xsl:value-of select="count(/dictionary/word/translation[@lang='FR'])"/> </p>
                    <p> Latein <xsl:value-of select="count(/dictionary/word/translation[@lang='LA'])"/> </p>
                    <h7> Created by Mario Skedelj </h7>
                </div>
            </footer>

            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>