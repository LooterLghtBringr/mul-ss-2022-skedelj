<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/lunchPlan">
    <html>
        <head>
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
                  integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
                  crossorigin="anonymous"/>
            <title>Menue Plan</title>
        </head>

        <body style="background-color:#f0ffff">
            <h1>Menue Plan</h1>
                <table>
                    <tr bgcolor="#00cccc">
                        <th>Weekday</th>
                        <th>Meal Name</th>
                        <th>Category</th>
                        <th>Area</th>
                        <th>Ingredients</th>
                    </tr>

                    <xsl:for-each select="dayOfWeek">
                    <xsl:choose>
                        <xsl:when test="category='Beef' or category='Chicken' or category='Pork'or category='Lamb'">
                            <tr bgcolor="#FF9900">
                                <td>
                                    <xsl:value-of select="@tag_ID"/>
                                </td>
                                <td>
                                    <xsl:value-of select="query"/>
                                </td>
                                <td>
                                    <xsl:value-of select="category"/>
                                </td>
                                <td>
                                    <xsl:value-of select="area"/>
                                </td>
                                <td>
                                    <xsl:for-each select="ingredient">
                                        <xsl:value-of select="text()"/>
                                        <xsl:if test="position()!=last()">, </xsl:if>
                                    </xsl:for-each>
                                </td>
                            </tr>
                        </xsl:when>
                        <xsl:when test="category='Vegetarian' or category='Vegan'">
                            <tr bgcolor="#99CC00">
                                <td>
                                    <xsl:value-of select="@tag_ID"/>
                                </td>
                                <td>
                                    <xsl:value-of select="query"/>
                                </td>
                                <td>
                                    <xsl:value-of select="category"/>
                                </td>
                                <td>
                                    <xsl:value-of select="area"/>
                                </td>
                                <td>
                                  <xsl:for-each select="ingredient">
                                      <xsl:value-of select="text()"/>
                                      <xsl:if test="position()!=last()">, </xsl:if>
                                  </xsl:for-each>
                                </td>
                            </tr>
                        </xsl:when>
                        <xsl:otherwise>
                            <tr bgcolor="#FFFFFF">
                                <td>
                                    <xsl:value-of select="@tag_ID"/>
                                </td>
                                <td>
                                    <xsl:value-of select="query"/>
                                </td>
                                <td>
                                    <xsl:value-of select="category"/>
                                </td>
                                <td>
                                    <xsl:value-of select="area"/>
                                </td>
                                <td>
                                  <xsl:for-each select="ingredient">
                                      <xsl:value-of select="text()"/>
                                      <xsl:if test="position()!=last()">, </xsl:if>
                                  </xsl:for-each>
                                </td>
                            </tr>
                        </xsl:otherwise>
                    </xsl:choose>
                    </xsl:for-each>
                </table>
        </body>

        <body>
            <style>
                body {font-size: 100%; font-family: Calibri;}
                table {border: solid 0.4em black;}
                h1 {font-size: 2em; font-family: Verdana;}
                h3 {font-size: 1.25em; font-family: Verdana; text-align: center; line-height: 1.25em;}
                th {font-size: 1.25em; font-family: Verdana; text-align: center; border: solid 0.1em black;}
                td {border: solid 0.1em black;}
                p {font-size: 0.75em; text-align: center; line-height: 25%;}
            </style>
            <br style="line-height: 3em;"/>
            <h3>Colour Code</h3>
            <p>
                <a style="background-color:#FF9900">Meal contains meat.</a>
            </p>
            <p>
                <a style="background-color:#99CC00">Meal is Vegetarian or Vegan.</a>
            </p>
            <p>
                <a style="background-color:#FFFFFF">Undefined.</a>
            </p>

        </body>
    </html>
</xsl:template>

</xsl:stylesheet>
