const SEARCH = "search.php";
const LIST = "list.php";
const RANDOM = "random.php";
const FILTER = "filter.php";
const LOOKUP = "lookup.php";
const CATEGORIES = "categories.php";
const SEARCH_ERROR = "Error during search!"
const LOAD_ERROR = "Error loading the ";

function init() {
    refreshMeals();
    createSaying();
    initEvents();
    fetchCategories().catch(err => {
        showError(LOAD_ERROR + "categories!");
        console.error(err);
    });
    fetchArea().catch(err => {
        showError(LOAD_ERROR + "areas!");
        console.error(err);
    });
    fetchIngredients().catch(err => {
        showError(LOAD_ERROR + "ingredients!");
        console.error(err);
    });
}

async function fetchCategories() {
    console.log("fetching categories...");
    const categories = await listCategories();

    createDropdownElement("dropdown_categories", "", "");
    categories.forEach(element => {
        createDropdownElement("dropdown_categories", element.strCategory, element.strCategory);
    });
}

async function fetchArea() {
    console.log("fetching areas...");
    const areas = await listAreas();

    createDropdownElement("dropdown_areas", "", "");
    areas.forEach(area => {
        createDropdownElement("dropdown_areas", area.strArea, area.strArea);
    });
}

async function fetchIngredients() {
    console.log("fetching ingredients...");
    const ingredients = await listIngredients();

    createDropdownElement("dropdown_ingredients", "", "");
    ingredients.forEach(element => {
        createDropdownElement("dropdown_ingredients", element.strIngredient, element.strIngredient);
    });
}

function initEvents() {
    document.getElementById("input_search").addEventListener("click", searchMealFromForm);
    document.getElementById("input_random").addEventListener("click", getRandomMeal);
    document.getElementById("myFile").addEventListener("change", handleFileLoad);
}

function filterResultsById(newMeals, previewsMeals) {
    const combinedArray = [];

    for (const newMeal of newMeals) {
        for (const previewsMeal of previewsMeals) {
            if (previewsMeal.idMeal === newMeal.idMeal) {
                combinedArray.push(previewsMeal);
            }
        }
    }
    return combinedArray;
}

async function search(searchText, ingredients, category, area) {
    let results = null;
    let noFullResult = false;

    if (searchText) {
        results = await searchByName(searchText);
    }

    if (ingredients) {
        for (const ingredient of ingredients) {
            if (results == null) {
                results = await searchByIngredient(ingredient);
                noFullResult = true;
            } else if (noFullResult) {
                let resultsByIngredients = await searchByIngredient(ingredient);
                results = filterResultsById(resultsByIngredients, results);
            } else {
                results = results.filter(result => filterIngredient(result, ingredient));
            }
        }
    }

    if (category.categoryName) {
        let savedResults = null;
        if (results == null) {
            results = await searchByCategory(category.categoryName);
            noFullResult = true;
        } else if (noFullResult) {
            let resultsByCategory = await searchByCategory(category.categoryName);
            savedResults = results;
            results = filterResultsById(resultsByCategory, results);
        } else {
            savedResults = results;
            results = results.filter(result => result.strCategory === category.categoryName);
        }
        if (category.secondary && savedResults != null && results.length === 0) {
            results = savedResults;
        }
    }

    if (area) {
        if (results == null) {
            results = await searchByArea(area);
        } else if (noFullResult) {
            let resultsByArea = await searchByArea(area);
            results = filterResultsById(resultsByArea, results);
        } else {
            results = results.filter(result => result.strArea === area);
        }
    }
    return results;
}

async function searchMealFromForm() {
    refreshMeals();
    let ingredients;
    const category = {
        categoryName: "",
        secondary: false
    };

    const searchText = document.getElementById("searchMealName").value;
    const ingredient = document.getElementById("dropdown_ingredients").value;
    const categoryName = document.getElementById("dropdown_categories").value;
    const area = document.getElementById("dropdown_areas").value;

    if (ingredient) {
        ingredients = [];
        ingredients.push(ingredient);
    }

    if (categoryName) {
        category.categoryName = categoryName;
    }

    if (!searchText && !ingredient && !categoryName && !area) {

        let notFoundText = document.createElement("p");
        notFoundText.innerText = "Did you really try to search without any parameters set?";
        notFoundText.id = "notFoundText";

        document.getElementById("firstCol").appendChild(notFoundText);
        return;
    }

    let results = null;

    try {
        results = await search(searchText, ingredients, category, area);
    } catch (err) {
        showError(SEARCH_ERROR);
        console.error(err);
        return;
    }


    if (results != null && results.length > 0) {
        let counter = 0;
        results.forEach(meal => {
            createListElement(counter, meal);
            counter++;
        });
    }
}

async function getRandomMeal() {
    refreshMeals();
    const results = await fetchRandomMeal();
    createRecipe(results[0]);
}

async function fetchRecipe(event) {
    console.log("fetching recipe information...");
    const meals = await searchByID(event.target.dataset.meal);
    if (meals != null && meals.length > 0) {
        createRecipe(meals[0]);
    }
}

function createSaying() {
    let sayingText = createTextElement("p", "&quot;So long as you have food in your mouth, you have solved all questions for the time being.&quot;");
    sayingText.id = "sayingText";

    let sayingAuthor = createTextElement("p", "Franz Kafka");
    sayingAuthor.id = "sayingAuthor";

    document.getElementById("firstCol").appendChild(sayingText);
    document.getElementById("secondCol").appendChild(sayingAuthor);
}

function createRecipe(_recipe) {
    refreshMeals();

    console.log("creating recipe...");
    let strMealThumb = document.createElement("img");
    strMealThumb.src = _recipe.strMealThumb;
    strMealThumb.id = "recipeThumbnail";

    let recipeInstructions = document.createElement("p");
    recipeInstructions.innerText = _recipe.strInstructions;

    document.getElementById("firstCol").appendChild(createTextElement("h3", _recipe.strMeal));
    document.getElementById("firstCol").appendChild(createTextElement("p", _recipe.strCategory + " / " + _recipe.strArea));
    document.getElementById("firstCol").appendChild(recipeInstructions);
    document.getElementById("secondCol").appendChild(strMealThumb);

    let i = 1;
    while (_recipe['strIngredient' + i]) {
        let strIngMsr = createTextElement("li", _recipe['strMeasure' + i] + ": " + _recipe['strIngredient' + i]);
        document.getElementById("firstCol").appendChild(strIngMsr);
        i++;
    }
}

function createTextElement(type, text) {
    let element = document.createElement(type);
    element.innerHTML = text;
    return element;
}

function filterIngredient(result, ingredient) {
    let i = 1;
    while (result['strIngredient' + i]) {

        if (result['strIngredient' + i] != null && result['strIngredient' + i].includes(ingredient)) {
            return true;
        }
        i++;
    }
    return false;
}

async function handleFileLoad(event) {
    refreshMeals();
    let files = event.target.files;
    let file = files[0];

    let text = '';
    try {
        text = await parse(file);
    } catch (err) {
        showError("Could not parse file!");
        console.error(err);
    }

    if (file.type === "application/json") {

        const object = JSON.parse(text);
        for (const dayOfWeek of object.lunchPlan) {
            let results;
            try {
                results = await search(dayOfWeek['query'], dayOfWeek.ingredient, dayOfWeek.category, dayOfWeek.area);
            } catch (err) {
                showError(SEARCH_ERROR);
                console.error(err);
            }

            printLunchPlan(dayOfWeek.dayOfWeek, results);
        }

    } else if (file.type === "text/xml") {
        const parser = new DOMParser();
        let xmlDoc = parser.parseFromString(text, "text/xml");
        const lunchPlan = xmlDoc.getElementsByTagName("lunchPlan")[0];
        for (const dayOfWeek of lunchPlan.children) {

            const query = dayOfWeek.getElementsByTagName("query")[0].textContent;
            const area = dayOfWeek.getElementsByTagName("area")[0].textContent;
            const ingredients = [];

            for (let ingredient of dayOfWeek.getElementsByTagName("ingredient")) {
                ingredients.push(ingredient.textContent);
            }

            const category = {
                categoryName: dayOfWeek.getElementsByTagName("category")[0].textContent,
                secondary: dayOfWeek.getElementsByTagName("category")[0].getAttribute("secondary")
            };
            let results;
            try {
                results = await search(query, ingredients, category, area);
            } catch (err) {
                showError(SEARCH_ERROR);
                console.error(err);
            }
            printLunchPlan(dayOfWeek.getAttribute("tag_ID"), results);
        }
    }
}

function printLunchPlan(dayOfWeek, results) {
    document.getElementById("firstCol").appendChild(createTextElement("h3", dayOfWeek));
    if (results.length === 0) {
        document.getElementById("firstCol").appendChild(createTextElement("p", "No meal found"));
    } else if (results.length === 1) {
        createListElement(0, results[0]);
    } else {
        const random = Math.floor(Math.random() * results.length);
        createListElement(0, results[random]);
    }
}


function parse(file) {
    // Always return a Promise
    return new Promise((resolve, reject) => {
        let content = '';
        const reader = new FileReader();
        // Wait till complete
        reader.onloadend = function (e) {
            resolve(e.target.result);
        };
        // Make sure to handle error states
        reader.onerror = function (e) {
            reject(e);
        };
        reader.readAsText(file);
    });
}


function createDropdownElement(_container, _dropdownItem, _category) {
    let dropdownItem = document.createElement("option");
    dropdownItem.innerHTML = _dropdownItem;
    dropdownItem.id = _category;
    document.getElementById(_container).appendChild(dropdownItem)
}

function createListElement(_counter, _meal) {
    console.log("creating list element...");
    let column;
    if (_counter % 2 === 0) {
        column = "firstCol";
    } else {
        column = "secondCol";
    }

    let item = createTextElement("h4", _meal.strMeal);
    item.setAttribute("data-meal", _meal.idMeal);
    item.addEventListener("click", fetchRecipe);
    document.getElementById(column).appendChild(item);
    let img = document.createElement("img");
    img.src = _meal.strMealThumb;
    img.alt = _meal.strMeal
    img.setAttribute("data-meal", _meal.idMeal);
    img.addEventListener("click", fetchRecipe);
    document.getElementById(column).appendChild(img);
}

function showError(message) {
    console.error(message);
    let alert = createTextElement("div", message);
    alert.setAttribute("class", "alert alert-danger alert-dismissible");
    let closeButton = document.createElement("button");
    closeButton.setAttribute("class", "btn-close");
    closeButton.addEventListener("click", (event) =>
        document.getElementById("liveAlertPlaceholder").removeChild(event.target.parentElement));
    alert.appendChild(closeButton);
    document.getElementById("liveAlertPlaceholder").appendChild(alert);
}

function refreshMeals() {
    document.getElementById("firstCol").innerHTML = "";
    document.getElementById("secondCol").innerHTML = "";
}

async function searchByName(name) {
    const responseObject = await fetchJson(SEARCH + "?s=" + name);
    return responseObject.meals;
}

async function searchByFirstLetter(firstLetter) {
    const responseObject = await fetchJson(SEARCH + "?f=" + firstLetter);
    return responseObject.meals;
}

async function searchByArea(area) {
    const responseObject = await fetchJson(FILTER + "?a=" + area);
    return responseObject.meals;
}

async function searchByCategory(category) {
    const responseObject = await fetchJson(FILTER + "?c=" + category);
    return responseObject.meals;
}

async function searchByIngredient(ingredient) {
    const responseObject = await fetchJson(FILTER + "?i=" + ingredient);
    return responseObject.meals;
}

async function listAreas() {
    const responseObject = await fetchJson(LIST + "?a=list");
    return responseObject.meals;
}

async function listCategories() {
    const responseObject = await fetchJson(LIST + "?c=list");
    return responseObject.meals;
}

async function listIngredients() {
    const responseObject = await fetchJson(LIST + "?i=list");
    return responseObject.meals;
}

async function listAllMealCategories() {
    const responseObject = await fetchJson(CATEGORIES);
    return responseObject.categories;
}

async function fetchRandomMeal() {
    const responseObject = await fetchJson(RANDOM);
    return responseObject.meals;
}

async function searchByID(id) {
    const responseObject = await fetchJson(LOOKUP + "?i=" + id);
    return responseObject.meals;
}

async function fetchJson(query) {
    let url = "https://www.themealdb.com/api/json/v1/1/"

    const response = await fetch(url + query);
    if (!response.ok) {
        throw new Error("Response not ok");
    }
    return response.json()
}

init();
