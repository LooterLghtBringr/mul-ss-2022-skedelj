function fetchDictionaryApi(){
    let api = "https://api.dictionaryapi.dev/api/v2/entries/en/hello";
    fetch(api).then(response => {
        return response.json();
    }).then(info => {
        console.log(info);
    });
}

function fetchDisneyApi(){
    let api = "https://api.disneyapi.dev/characters";
    fetch(api).then(response => {
        return response.json();
    }).then(info => {
        console.log(info);
    });
}

function fetchCoinCapApi(){
    let api = "https://api.coincap.io/v2/assets";
    fetch(api).then(response => {
        return response.json();
    }).then(info => {
        console.log(info);
    });
}

function fetchNasaApi(){
    let api = "https://api.nasa.gov/techtransfer/patent/?engine&api_key=DEMO_KEY";
    fetch(api).then(response => {
        return response.json();
    }).then(info => {
        console.log(info);
    });
}

fetchDictionaryApi();