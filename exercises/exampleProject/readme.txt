Wie könnte json aussehen? Was erwarten wir uns von der DB als return value?

-Name
-Pers#
-Datum
-Beginn einer Buchung
-Ende einer Buchung
-Dauer der Buchung (Differenz aus Ende und Beginn)
-Kostenstelle (Zahlenwert)
-Information, ob es sich um eine verrechenbare oder nicht-verrechenbare Buchunghandelt (Flag)

Wie könnten endpoints dazu aussehen?

-api/mitarbeiter/
-api/mitarbeiter/personalnummer

andere Beispiel-Endpoints:
-api/mitarbeiter/personalnummer?date=2022-03-14
-api/mitarbeiter/personalnummer/kostenstelle/kostenstellennummer

-api/timerecords/employees/personalnummer
-api/timerecords/kostenstelle/kostenstellennummer